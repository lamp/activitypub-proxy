# ActivityPub Proxy

A brutally-simple proxy for ActivityPub that lets you circumvent instance blocks by masquerading as another domain name. All it does is replace all hostnames in the text proxied through, and for signed POST requests, it swaps the public keys and re-signs the requests.

Suppose you have a server at *.proxy.example, your handle is @bob@mastodon.one and you want to follow a friend at @alice@mastodon.two, but mastodon.two blocks mastodon.one for irrelevant reasons. You can change the handle to @alice@mastodon-two.proxy.example and theoretically will be able to fully interact with the other user (and vice-versa) by being masqueraded as @bob@mastodon-one.proxy.example. (If the domain has hyphens, replace with double hyphens.)

The intended usage of this is as an alternative to using alt accounts or moving your account to circumvent whole-server blocks that have nothing to do with you and are unfairly cutting you off from mutuals, despite your particular account being compliant with their rules. Of course, you are entirely responsible for your behavior and compliance whether it's an alt account or a mirror, as there is no practical difference to the other end. ActivityPub Proxy is not intended for nefarious usage.

The major caveat with this particular implementation is that if you boost or reply to proxied posts, your followers will see and interact with those authors via masquerade as well, causing a bit of a mess. This is unless you limit the usage to whitelisted domains/users, which you will almost definitely have to do or someone will abuse it and get it blocked. In that case, anyone who isn't whitelisted just won't see the boosts.


## Installation

You will need a host with Node.js 15 or newer, and a wildcard domain with HTTPS pointed to your server. Cloudflare may be easiest, as you can bind the app to an extra IP address and connect Cloudflare directly to it.

Download the repository and `npm i`. Then you can run it with the following environment variables (you can create a .env file).

- `PORT`: The port to listen for HTTP, default: 80.
- `BIND_IP`: The IP address to bind to. Default: all.
- `DOMAIN_WHITELIST`: Comma-separated list of domains that can be proxied. Recommended to use this to prevent abuse as otherwise anyone can proxy anything. Remember to include the domains you want to follow from as well as the domains you want to follow, as it needs to work both ways. Default: any
- `USER_WHITELIST`: Comma-separated list of usernames that can be proxied (case-sensitive, name only without `@`). Default: any
- `NODE_ENV`: Set to `development` to see debug logs.

Install by copying and pasting this example systemd file to `/etc/systemd/system/ap-proxy.service` (or similar), and editing as needed:

```systemd
[Unit]
Description=Simple ActivityPub Proxy
Documentation=https://gitea.moe/lamp/activitypub-proxy
After=network.target

[Service]
Environment= PORT=80 BIND_IP=0.0.0.0 DOMAIN_WHITELIST=mastodon.social,mstdn.social
WorkingDirectory=/path/to/activitypub-proxy/
ExecStart=/usr/bin/node .

[Install]
WantedBy=multi-user.target
```

`systemctl enable --now ap-proxy` and Bob's your uncle.
